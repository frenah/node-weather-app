const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const request = require('request');

//allows access to the public folder
app.use(express.static('public'));
//no idea, something about reading the body
app.use(bodyParser.urlencoded({ extended: true }));
//sets the view engine/templating stuff to files ending in ejs?
app.set('view engine', 'ejs');


//request made to route will be given index (from views)
app.get('/', function (req, res) {
  res.render('index', {weather: null, error: null});
});

//when a post is made render, do request, render index with response stuff
app.post('/', function (req, res) {
  let apiKey = "123nothing2c";
  let city = req.body.city;
  let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      res.render('index', {weather: null, error: 'Error, please try again'});
    } else {
      let weather = JSON.parse(body)
      if(weather.main == undefined){
        res.render('index', {weather: null, error: 'Error, please try again'});
      } else {
        let weatherText = `It's ${weather.main.temp} degrees in ${weather.name}!`;
        res.render('index', {weather: weatherText, error: null});
      }
    }
  });
})

//if I get a request for 3000, I'll console.log this?
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
